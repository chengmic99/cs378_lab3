// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/SphereComponent.h"
#include "TriggerActor.generated.h"
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FTriggerDelegate);
UCLASS()
class CS378_LAB3_API ATriggerActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATriggerActor();
	FORCEINLINE class UStaticMeshComponent* GetMeshComponent() const { return MeshComponent; }
	FORCEINLINE class USphereComponent* GetTriggerComponent() const { return TriggerComponent; }
	FTriggerDelegate OnTriggerDelegate;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UStaticMeshComponent* MeshComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class USphereComponent* TriggerComponent;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
