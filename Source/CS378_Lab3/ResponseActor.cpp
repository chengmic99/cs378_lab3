// Fill out your copyright notice in the Description page of Project Settings.


#include "ResponseActor.h"
#include "Components/StaticMeshComponent.h"

// Sets default values
AResponseActor::AResponseActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	MeshComponent->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
	//triggerActor = (ATriggerActor*) NULL;
}

// Called when the game starts or when spawned
void AResponseActor::BeginPlay()
{
	//Super::BeginPlay();
	if(triggerActor)
		triggerActor->OnTriggerDelegate.AddDynamic(this, &AResponseActor::Respond);
}
void AResponseActor::Respond() {
	if (GEngine)
		GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("Responding..."));
	FVector newLocation = GetActorForwardVector() * 100 + GetActorLocation();
	MeshComponent->SetWorldLocation(newLocation);

}
// Called every frame
void AResponseActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

