// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TriggerActor.h"
#include "ResponseActor.generated.h"

UCLASS()
class CS378_LAB3_API AResponseActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AResponseActor();
	FORCEINLINE class UStaticMeshComponent* GetMeshComponent() const { return MeshComponent; }
	UFUNCTION(BlueprintCallable)
		void Respond();
	UPROPERTY(Category = ResponseActor, EditAnywhere, BlueprintReadWrite)
		class ATriggerActor* triggerActor;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UStaticMeshComponent* MeshComponent;
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
