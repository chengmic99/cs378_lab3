// Copyright Epic Games, Inc. All Rights Reserved.


#include "CS378_Lab3GameModeBase.h"
#include "WorldPawn.h"

ACS378_Lab3GameModeBase::ACS378_Lab3GameModeBase() {
	static ConstructorHelpers::FObjectFinder<UClass> pawnBPClass(TEXT("Blueprint'/Game/Blueprints/WorldPawnBP.WorldPawnBP_C'"));

	if (pawnBPClass.Object) {
		if (GEngine)
			GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Red, TEXT("PawnBP Found"));
		UClass* pawnBP = (UClass*)pawnBPClass.Object;
		DefaultPawnClass = pawnBP;
	}
	else {
		if (GEngine)
			GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Red, TEXT("PawnBP Not Found"));
		DefaultPawnClass = AWorldPawn::StaticClass();
	}
}

